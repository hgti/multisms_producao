<?php
/**
 * 25/04/2017
 * Funcoes de comunicacao do gateway
 * Criado por: Hudson Claudino
 *
 */

function post_data($url, $data, $username, $password)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
    curl_setopt($curl, CURLOPT_USERPWD,"$username:$password");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    print_r(error_get_last());

    $json_response = curl_exec($curl);
    print_r(error_get_last());
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return array($status, $json_response);
}

/**
 * get data from url using GET method
 * 
 * @param string $url, the remote URL, (e.g. 'http://172.16.222.22:2222/api/query_sms_result')
 * @param string $username, authenticated username, e.g. 'admin'
 * @param string $password, authenticated password, e.g. 'admin'
 * @return array ($status, $response), e.g. success ? ('200 OK', $json_response), not consider fail now
 */
function get_data($url, $username, $password)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");

    $json_response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return array($status, $json_response);
}


?>