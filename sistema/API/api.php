<?php
/**
 * post data to url using POST method
 *
 * @param string $url, the url which received the data (e.g. 'http://172.16.222.22:2222/api/send_sms')
 * @param array $data, for sms, this is json format, constructed by send_sms_content, etc.
 * @param string $username, username for web of gateway. (e.g. 'admin')
 * @param string $password, password for web of gateway. (e.g. 'admin')
 * @return array ($status, $response), e.g. success? ("200 OK", $json_response), not consider failed now.
 */
function post_data($url, $data, $username, $password)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
    curl_setopt($curl, CURLOPT_USERPWD,"$username:$password");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    print_r(error_get_last());

    $json_response = curl_exec($curl);
    print_r(error_get_last());
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return array($status, $json_response);
}

/**
 * get data from url using GET method
 * 
 * @param string $url, the remote URL, (e.g. 'http://172.16.222.22:2222/api/query_sms_result')
 * @param string $username, authenticated username, e.g. 'admin'
 * @param string $password, authenticated password, e.g. 'admin'
 * @return array ($status, $response), e.g. success ? ('200 OK', $json_response), not consider fail now
 */
function get_data($url, $username, $password)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");

    $json_response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return array($status, $json_response);
}

/**
 * construct the send sms data which will be used by the post_data.
 *
 * @param string $text, this is the sms content. e.g. "Hello, how are you sir."
 * @param string $number, the sms recepient number. e.g. '10086'
 * @param int $user_id, this is unique for every sms. e.g. "sms0", user_id=0; "sms1", user_id=1; etc
 * @param int $port, the specific port number to send the sms. e.g. $port = 1.
 * @param string $encoding, specify the encoding of sms. supports "unicode" and 'gsm-7bit' only
 * @return array (json) $data. will be formatted as the API manual described. see more in manual.
 */
function send_sms_data($text, $number, $user_id, $port = NULL, $encoding = NULL)
{
    $data = array
    (
        "text" => "#param#",
        "param" => array
        (
            array(
            "number" => $number,
            "text_param" => array($text),
            "user_id" => $user_id,
            ),
        ),
    );
    if ($port != NULL) {
        $data["port"] = array($port);
    }

    if ($encoding != NULL) {
        $data["encoding"] = $encoding;
    }
    return $data;
}

/**
 * @param string array $number, the number array. e.g. {'10086', '10010'}
 * @param unsigned int array $port, the port number array. e.g. {0,1,2}
 * @param string $time_after, the string format time which specify the start of time. e.g. "2015-12-22 10:11:59"
 * @param string $time_before, the string format time which specify the end of the time. 
 * @param unsigned int array $user_id, the user_id of the sms which is from the send_sms_data. e.g. {1,2}
 * @return json format array $data, formated the param in the format which is described in the API manual.
 */
function query_sms_result_data($number, $port, $time_after, $time_before, $user_id)
{
    $data = array();
    if ($number != NULL) {
        $data["number"] = array($number);
    }
    if ($port != NULL) {
        $data["port"] = array($port);
    }
    if ($time_after != NULL) {
        $data["time_after"] = $time_after;
    }
    if ($time_before != NULL) {
        $data["time_before"] = $time_before;
    }
    if ($user_id != NULL) {
        $data["user_id"] = array($user_id);
    }

    return $data;
}

function query_sms_delivery_status_data($number, $port, $time_after, $time_before)
{
    $data = array();
    if ($number != NULL) {
        $data["number"] = array($number);
    }
    if ($port != NULL) {
        $data["port"] = array($port);
    }
    if ($time_after != NULL) {
        $data["time_after"] = $time_after;
    }
    if ($time_before != NULL) {
        $data["time_before"] = $time_before;
    }
    return $data;
}

$username = 'admin';
$password = 'admin';


$send_url = 'http://192.168.1.5/api/send_sms';
$result_url = 'http://192.168.1.5/api/query_sms_result';
$delivery_url = 'http://192.168.1.5/api/query_sms_deliver_status';
$queue_url = 'http://192.168.1.5/api/query_sms_in_queue';

/*
$send_url = 'http://multisms1.ddns.net/api/send_sms';
$result_url = 'http://multisms1.ddns.net/api/query_sms_result';
$delivery_url = 'http://multisms1.ddns.net/api/query_sms_deliver_status';
$queue_url = 'http://multisms1.ddns.net/api/query_sms_in_queue';
*/
// montagem da estrutura a ser enviada para a chipeira
$send_data = send_sms_data("TESTANDO PORTA 1", "11958130042", 1, 5, "unicode");

echo "dump send data array:<BR>";
////print_r($send_data);

//var_dump($send_data);
////$query_result_data = query_sms_result_data("10086",7,NULL, NULL, 1);
////echo "dump query result data array:\n";
////print_r($query_result_data);
//var_dump($query_result_data);
////$query_delivery_data = query_sms_delivery_status_data("10086", 7, NULL, NULL);
//echo "dump query delivery  data array:\n";
//print_r($query_delivery_data);
//var_dump($query_delivery_data);

// now we are going to send a sms which is in $data, the user_id is 1.
$res = post_data($send_url, $send_data, $username, $password);
// check the result.
if ($res[0] == 200) {
    echo $res[1];
} else {
// some error occur
echo "Error:".$res[1];
}


// now sleep for 5 seconds
sleep(5);

// then check the result
/*
$res = post_data($result_url, $query_result_data, $username, $password);
if ($res[0] == 200) {
    echo $res[1];
} else {
    echo "Error:".$res[1];
}

// sleep another 5 seconds.
sleep(5);
$res = post_data($delivery_url, $query_delivery_data, $username, $password);
if ($res[0] == 200) {
    echo $res[1];
} else {
   echo "Error:".$res[1];
}
*/
?>
<a href="http://multisms.com.br/API/queue.php">Fila de espera</a>
