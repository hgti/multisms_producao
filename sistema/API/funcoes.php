<?php
/**
 * 25/04/2017
 * Funcoes de envio e retorno do gateway
 * Criado por: Hudson Claudino
 *
 **/

/**
 * construct the send sms data which will be used by the post_data.
 *
 * @param string $text, this is the sms content. e.g. "Hello, how are you sir."
 * @param string $number, the sms recepient number. e.g. '10086'
 * @param int $user_id, this is unique for every sms. e.g. "sms0", user_id=0; "sms1", user_id=1; etc
 * @param int $port, the specific port number to send the sms. e.g. $port = 1.
 * @param string $encoding, specify the encoding of sms. supports "unicode" and 'gsm-7bit' only
 * @return array (json) $data. will be formatted as the API manual described. see more in manual.
 */
function send_sms_data($text, $number, $user_id, $port = NULL, $encoding = NULL)
{
    $data = array
    (
        "text" => "#param#",
        "param" => array
        (
            array(
            "number" => $number,
            "text_param" => array($text),
            "user_id" => $user_id,
            ),
        ),
    );
    if ((!$port == NULL)) {
        $data["port"] = array($port);
    }

    if ($encoding != NULL) {
        $data["encoding"] = $encoding;
    }
    return $data;
}

/**
 * @param string array $number, the number array. e.g. {'10086', '10010'}
 * @param unsigned int array $port, the port number array. e.g. {0,1,2}
 * @param string $time_after, the string format time which specify the start of time. e.g. "2015-12-22 10:11:59"
 * @param string $time_before, the string format time which specify the end of the time. 
 * @param unsigned int array $user_id, the user_id of the sms which is from the send_sms_data. e.g. {1,2}
 * @return json format array $data, formated the param in the format which is described in the API manual.
 */
function query_sms_result_data($number, $port, $time_after, $time_before, $user_id)
{
    $data = array();
    if ($number != NULL) {
        $data["number"] = array($number);
    }
    if ($port != NULL) {
        $data["port"] = array($port);
    }
    if ($time_after != NULL) {
        $data["time_after"] = $time_after;
    }
    if ($time_before != NULL) {
        $data["time_before"] = $time_before;
    }
    if ($user_id != NULL) {
        $data["user_id"] = array($user_id);
    }

    return $data;
}

function query_sms_delivery_status_data($number, $port, $time_after, $time_before)
{
    $data = array();
    if ($number != NULL) {
        $data["number"] = array($number);
    }
    if ($port != NULL) {
        $data["port"] = array($port);
    }
    if ($time_after != NULL) {
        $data["time_after"] = $time_after;
    }
    if ($time_before != NULL) {
        $data["time_before"] = $time_before;
    }
    return $data;
}

?>