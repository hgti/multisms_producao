<?php
/**
 * 25/04/2017
 * Parametrizações do gateway de envio
 * Criado por: Hudson Claudino
 *
 */

$username = 'admin';
$password = 'admin';

/*******    LOCALHOST
*/

if ($ambiente == 'DEV'){
    $send_url = 'http://192.168.0.10/api/send_sms';
    $result_url = 'http://192.168.0.10/api/query_sms_result';
    $delivery_url = 'http://192.168.0.10/api/query_sms_deliver_status';
    $queue_url = 'http://192.168.0.10/api/query_sms_in_queue';
}else{
/*******    PRODUCAO  */
    $send_url = 'http://187.37.243.192/api/send_sms';
    $send_url = 'http://187.37.243.192/api/send_sms';
    $result_url = 'http://187.37.243.192/api/query_sms_result';
    $delivery_url = 'http://187.37.243.192/api/query_sms_deliver_status';
    $queue_url = 'http://187.37.243.192/api/query_sms_in_queue';
}
?>