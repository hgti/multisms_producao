<?php
/**
 * 25/04/2017
 * Programa para envio simples de SMS
 * Criado por: Hudson Claudino
 *
 **/

/* DEBUG */

/* */
//INCLUDE AREA
//conecta base de dados
require '../config.php';
require 'db/sms.php';
require 'db/sms_pendentes.php';
require 'db/login.php';
require 'db/campanha.php';
require 'db/usuario.php';
require 'db/porta.php';
require 'db/consumo.php';
require 'php/valida.php';

//acesso gateway
require 'API/param.php';
require 'API/postget.php';
require 'API/funcoes.php';

//retirar a linha abaixo quando realizar teste integrado
//$_SESSION['login'] = 'teste';

if (!validaLogin()){
    header("Location: ../proibido.html");
}

//Validação de preenchimento de tela
if ((!empty($_POST['nome'])) or (!empty($_POST['fone'])) ){

  /* DEBUG 
  $fone = '11958130042';
  $nome = 'Hudson';
  $msg = 'Teste';
  */
  
  $nome = $_POST['nome'];
  $fone = $_POST['fone'];
  $msg = $_POST['msg'];
  /* */

  //select usuario
  $login = $_SESSION['login'];
  $result = buscaUsuario($conn, $login);
  $user_id = $result['usuario_id'];

  //dados da mensagem  
  $smsfone = preg_replace( '/[^\d]+/' , '' , $fone );
  $smsnome = $nome;
  $smsmensagem = $msg;

  //prepara dados para insert da tabela
  $usuariocnpj = $result['Cliente_cliente_CNPJ'];
  $lastid = recuperaMax($conn, $usuariocnpj);
  ++$lastid;
  $idtipocliente = $result['Cliente_TipoCliente_tipocliente_id'];
  $categcliente = $result['Cliente_CategoriaCliente_categoriacliente_id'];
  $idusuario = $user_id;
  $idcampanha = 99; // hardcode de dados da campanha por ser sms unitário
  $statuscampanha = 2; // hardcode para Status "Finalizada"
  $statussms = 3; // hardcode para status "Enviado"
  $smsoperadora = null; 

  //verifica porta livre para envio
  $res_porta = verifica_portas($conn, $tempoespera, $qtdsmspermitida);
  if ($res_porta != null){
    $porta = $res_porta['porta_num'];
    $gateway = $res_porta['Gateway_gateway_id'];
    $qtdemuso = $res_porta['porta_qtdemuso'];  
    $utilizado_mes = $res_porta['porta_utilizado_mes'];
    /* DEBUG 
    //var_dump($res_porta);
    echo "Porta: ".$porta."<br>";
    echo "Gateway: ".$gateway."<br>";
    /* */

    // monta $data -- tive de parsear a porta. A API só aceita o tipo inteiro para envio, senão considera null
    $senddata = send_sms_data($smsmensagem, $smsfone, $idusuario, (int)$porta, "unicode");
    $url = $send_url;
    /* DEBUG 
    echo "URL: " .$url."<br>";
    echo "Data: ";
    var_dump($senddata);
    echo "<br> ";
    echo "Username :" .$username."<br>";
    echo "Senha: " .$password."<br>";
    /*  */

    //chama envio de sms - Comentado para não enviar
    $response = post_data($url, $senddata, $username, $password);

    /* DEBUG 
    echo "<br>";
    echo "envio de mensagem <br>";
    echo "Fone : ".$smsfone." <br>";
    echo "Nome : ".$smsnome." <br>";
    echo "Mens : ".$smsmensagem. " <br>";
    echo "Porta: ".$porta. " <br>";
    echo "url  : ".$url."<br>";
    echo "<br>";
    var_dump($response);
    echo "<br>";
     echo "Data: ";
    var_dump($senddata);
    echo "<br> ";
    /* */ 

    if ($response[0] == 200) {
        // Enviado com sucesso para fila de envio - Status na tabela SMS status como pendente
        /* DEBUG
        echo "Mensagem :".$response[1];
        echo "<br>";
        /**/
        //grava na tabela SMS 
        insereSMS($conn, $lastid, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, $gateway, $porta, $smsfone, $smsnome, $smsoperadora, $smsmensagem);
      
        //atualiza qtd em uso
        if($qtdemuso < $qtdsmspermitida){
            ++$qtdemuso;
        } else {
            $qtdemuso = 1;
        }
        ++$utilizado_mes;
        $resupd = atualizaporta($conn, $gateway, $porta, $qtdemuso, $utilizado_mes);
        if($resupd){
          //echo "Erro ao alterar tabela Porta <br>";
        } //else  echo "Se chegou aqui alterou a porta com sucesso

        atualizaDataCampanha($conn, $idcampanha, $usuariocnpj);
        /*
        $resultconsumo = buscaConsumo($conn, $usuariocnpj);
        $consumo_qtd_usada = $resultconsumo['consumo_qtd_usada'] + 1; 
        echo 'CNPJ: '.$usuariocnpj.'<br>';
        echo 'ID consumo: '.$resultconsumo['consumo_id'].'<br>';
        echo 'Consumo: '.$resultconsumo['consumo_valor'].'<br>';
        echo 'Qtd Usada: '.$consumo_qtd_usada.'<br>';
        alteraConsumo($conn, $resultconsumo['consumo_id'], $resultconsumo['consumo_valor'], $consumo_qtd_usada);
        */

    } else {
  // Mensagem de erro
    echo "<script> alert('SMS não enviado. problema de comunicação com o Gateway. Por favor contacte o suporte do sistema.');</script>";
//    echo "SMS não enviado - Cod.:" .$response[1];
  }

  }else{
    // não há portas disponíveis para envio. Enviando para base sms_pendentes para ser enviado posteriormente
    $statussms = 1; // hardcode para status "Pendente"
    $statuscampanha = 1;// hardcode para status "Pendente"
    $resultsmspend = insereSMSPend($conn, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, 99, 9999, $smsfone, $smsnome, $smsoperadora, $smsmensagem);
    //echo 'Mensagem em processamento';
  }//fim do if de verificação de portas
  

$senddata = $response = null;
}// fim da validação dados passados por tela
// inicio HTML
require 'enviosimples.html';
?>