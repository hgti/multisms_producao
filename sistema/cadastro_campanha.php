<?php
require '../config.php';
require 'php/valida.php';
require 'db/usuario.php';
require 'db/campanha.php';
require 'db/sms_pendentes.php';

//retirar a linha abaixo quando realizar teste com as outras páginas
//$_SESSION['login'] = 'teste';

if (!validaLogin()){
    header("Location: ../proibido.hmtl");
}

//Validação de preenchimento de tela
if (!empty($_POST['nomeCamp'])){

    $delimitador = ',';

    // Abertura e leitura de arquivo

    /* DEBUG 
    $arquivoDebug = $_POST['arquivoTXT']; 
    $f = fopen ($arquivoDebug['tmp_name'], 'r');
    /**/

    try {
        $nome_temporario=$_FILES["arquivoTXT"]["tmp_name"]; 
        //echo '<br> '.$nome_temporario.'<br>';
        $nome_real=$_FILES["arquivoTXT"]["name"]; 
        //echo '<br> '.$nome_real.'<br>';
        copy($nome_temporario,$nome_real); 
        $arquivo = $nome_real; 
        $f = fopen ($arquivo, 'r');

        if ($f) { 
            // Enquanto nao terminar o arquivo ou encontrar um erro de validacao
            $Erro = false;
            while (!feof($f)) { 
                // Ler uma linha do arquivo
                $linha = fgetcsv($f, 0, $delimitador);
                if (!$linha) {
                    continue;
                }
                // DEBUG Exibindo registros
                //echo "Linha->Nome: " .$linha[0]. "<br>" ;

                //validacoes dos campos
                $Erro = validarMax($linha[0], 50); // Nome
                if ($Erro == true){
                    throw new Exception('Arquivo com registro errado: Campo Nome inválido.');
                    /*
                    $codErro = '01';
                    break;
                    */
                }
                $Erro = validarCelular($linha[1]); // Numero
                if ($Erro == true){
                    throw new Exception('Arquivo com registro errado: Campo Numero inválido.');
                    /* DEBUG 
                    $codErro = '02';
                    break;
                    */
                }
            }//fim Loop
            fclose($f);
        }

        // Processamento
        $login = $_SESSION['login'];
        $usuario = buscaUsuario($conn, $login);

        $idusuario = $usuario['usuario_id'];
        $usuariocnpj = $usuario['Cliente_cliente_CNPJ'];
        $categcliente = $usuario['Cliente_CategoriaCliente_categoriacliente_id'];
        $idtipocliente = $usuario['Cliente_TipoCliente_tipocliente_id'];
        // grava Campanha
        $statuscampanha = 1; //hardcode para status "Em processamento"
        $nomecampanha = $_POST['nomeCamp'];

        $idcampanha = recuperaMaxCamp($conn, $usuariocnpj);
        ++$idcampanha; // não utilizo o autoincrement para poder aproveitar o id no insert do sms
        
        /*DEBUG
        echo 'id_campanha '.$idcampanha .'<br>';
        echo 'idusuario'.$idusuario.'<br>';
        echo 'usuariocnpj'.$usuariocnpj.'<br>';
        echo 'categcliente'.$categcliente.'<br>';
        echo 'idtipocliente'.$idtipocliente.'<br>';
        echo 'statuscampanha'.$statuscampanha.'<br>';
        echo 'nomecampanha'.$nomecampanha.'<br>';
        /**/
        
        
        
        $resultcamp = insereCampanha($conn, $idcampanha, $idusuario, $usuariocnpj, $categcliente, $idtipocliente, $statuscampanha, $nomecampanha);
        if ($resultcamp) {
            throw new Exception('Erro na inclusão da campanha');

        }

        /* DEBUG 
        if ($resultcamp == true){
            // deu ruim
            echo "Erro de cadastro de campanha";
        }else{
            echo "campanha cadastrada";
        }*/

        if ($Erro == false){
        // grava SMS_Pendentes
        /* DEBUG 
            echo "Area de envio de SMS <br>";
        */
            $statussms = 1;
            $smsoperadora = null;
            $smsmensagem = $_POST['msg'];

            //loop para insert
            $f = fopen ($arquivo, 'r');
            if ($f) { 

            // Enquanto nao terminar o arquivo
                while (!feof($f)) { 
                // Ler uma linha do arquivo
                    $linha = fgetcsv($f, 0, $delimitador);
                    if (!$linha) {
                        continue;
                    }
                    $smsnome = $linha[0];
                    $smsfone = $linha[1];
                    $resultsmspend = insereSMSPend($conn, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, 99, 9999, $smsfone, $smsnome, $smsoperadora, $smsmensagem);
                    if ($resultsmspend) {
                        throw new Exception('Erro na inclusão do SMS.');
                        /* DEBUG 
                        $Erro = true;
                        $codErro = '04';
                        break;
                        */
                    }
                    /* DEBUG
                    if (!$resultsmspend){
                        //INSERT OK
                        echo "<br>";
                        echo "Insert SMS realizado com sucesso";
                        echo "<br>";
                    }
                    else{
                        //deu ruim
                        $Erro = true;
                        echo "<br>";
                        echo "Erro de insert SMS";
                        echo "<br>";
                    }
                    */
                }//fim While
            }
            echo "<script> alert('Campanha cadastrada! Por favor consulte a gestão de campanhas'); </script>";
        }
 //fim try
    } catch (Exception $e) {
    echo 'Erro ocorrido : ',  $e->getMessage(), "\n";
} 

/* retirado devido tratamento com TryCatch - Exception
    // Mensagens de erro
    if ($Erro == true){
        switch ($codErro) {
        case '01':
            echo 'Erro no arquivo txt. Campo Nome inválido.';
            break;
        case '02':
            echo 'Erro no arquivo txt. Campo Numero inválido.';
            break;
        case '03':
            echo 'Erro na inclusão da campanha';
            break;
        case '04':
            echo 'Erro ma inclusão do SMS';
            break;
        default: echo 'Erro inesperado. Entre em contato com o suporte';
        }
    }
*/

} // fim do IF de validacao da Pag

require 'cadastro_campanha.html';
?>