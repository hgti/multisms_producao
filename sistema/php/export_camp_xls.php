<?php
// Incluimos a classe PHPExcel
include  'xls/PHPExcel.php';

// Instanciamos a classe
$objPHPExcel = new PHPExcel();

// Definimos o estilo da fonte
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

// Define a largura das colunas de modo automático
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);


// Criamos as colunas
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A1", "CAMPANHA" )
            ->setCellValue("B1", "ENVIADOS " )
            ->setCellValue("C1", "FALHAS" )
            ->setCellValue("D1", "PENDENTES" )
            ->setCellValue("E1", "STATUS" )
            ->setCellValue("F1", "USUARIO" );

// Define a planilha ativa para o PHPExcel operar
$objPHPExcel->setActiveSheetIndex(0);

// Define o título da planilha 
$nomearq = "ExporCampanhas";
$objPHPExcel->getActiveSheet()->setTitle('Planilha de Credeciamento 1');



// Exemplo de preenchimento de dados de maneira dinâmica, a partir de um resultado do banco de dados por exemplo.
$credenciados = $wpdb->get_results($query);
$linha = 3; 
foreach ($credenciados as $key => $credenciado) {
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("B". $linha, $credenciado->nome)
        ->setCellValue("C". $linha, $credenciado->sobrenome)
        ->setCellValue("D". $linha, $credenciado->email);
    $linha++;
}



?>
