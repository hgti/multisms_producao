<?php
require '../../config.php';
require '../db/campanha.php';
require '../db/sms.php';
require '../db/sms_pendentes.php';

$idcampanha = $_REQUEST["idcampanha"];
    //echo 'CNPJ: '.$usuariocnpj.'<br>';
    //Consultando banco de dados
  
$qtdEnviados = contaSMS($conn, $idcampanha, 3);

// lista qtd SMS falha
$qtdFalha = contaSMS($conn, $idcampanha, 4);

// lista qtd SMS_PENDENTES
$qtdPendentes = contaPendporCamp($conn, $idcampanha, 1);
$qtdEntregues = $qtdEnviados - $qtdFalha;

$json_str = '{"qtdEnviados":'.$qtdEnviados.',"qtdFalha":'.$qtdFalha.',"qtdEntregues":'.$qtdEntregues.', "qtdPendentes":'.$qtdPendentes.'}';

//Passando vetor em forma de json

$myJSON = json_encode($json_str);
echo $myJSON;
?>