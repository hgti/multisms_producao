<?php
require '../config.php';
require 'db/cliente.php';
require 'db/categoria_cliente.php';
require 'db/tipo_cliente.php';

$result = listaClientesAtivos($conn);
//print_r($result);

if (count($result) > 0) {
    echo "<table>
            <tr>
                <th>cliente_CNPJ</th>
                <th>Categoria</th>
                <th>Tipo de Cliente</th>
                <th>cliente_nome</th>
                <th>cliente_IM</th>
                <th>cliente_IE</th>
                <th>cliente_IsencaoIE</th>
                <th>cliente_CEP</th>
                <th>cliente_end</th>
                <th>cliente_end_num</th>
                <th>cliente_end_comp</th>
                <th>cliente_bairro</th>
                <th>cliente_cidade</th>
                <th>cliente_estado</th>
                <th>cliente_pais</th>
                <th>cliente_telefone</th>
                <th>cliente_email</th>
                <th>cliente_site</th>
            </tr>";
    foreach ($result as $row) {
        //busca categoria 
        $categoria = buscaCategoriaCliente($conn, $row["CategoriaCliente_categoriacliente_id"]);
        //busca tipo
        $tipocliente = buscaTipoCliente($conn, $row["TipoCliente_tipocliente_id"]);
    echo   "<tr>
                <td>".$row["cliente_CNPJ"]."</td>
                <td>".$categoria["categoriacliente_descricao"]."</td>
                <td>".$tipocliente["tipocliente_descricao"]."</td>
                <td>".$row["cliente_nome"]."</td>
                <td>".$row["cliente_IM"]."</td>
                <td>".$row["cliente_IE"]."</td>
                <td>".$row["cliente_IsencaoIE"]."</td>
                <td>".$row["cliente_CEP"]."</td>
                <td>".$row["cliente_end"]."</td>
                <td>".$row["cliente_end_num"]."</td>
                <td>".$row["cliente_end_comp"]."</td>
                <td>".$row["cliente_bairro"]."</td>
                <td>".$row["cliente_cidade"]."</td>
                <td>".$row["cliente_estado"]."</td>
                <td>".$row["cliente_pais"]."</td>
                <td>".$row["cliente_telefone"]."</td>
                <td>".$row["cliente_email"]."</td>
                <td>".$row["cliente_site"]."</td>
           <tr>";
    }
} else {
    echo "0 registros encontrados";
}


?>