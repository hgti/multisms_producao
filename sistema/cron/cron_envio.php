<?php
//para teste
//file_put_contents("teste.txt", "Crontab rodou em: ".date('d/m/Y H:i'));
/* RETIRADO PARA INTEGRACAO DO CRON
require '../config.php';
require '../db/campanha.php';
require '../db/sms_pendentes.php';
require '../db/erros.php';
require '../API/param.php';
require '../API/funcoes.php';
require '../API/postget.php';
require '../php/verifica_portas.php';
require '../php/logs.php';
*/
    $nomeLog = "CronEnvio_".date('dmY').".log";
//    echo 'CronEnvio <br>';
// LISTA CAMPANHAS
    $result = listaCampanhaPend($conn);

    $qtdcampanhas = count($result);
    logMsg( "Qtd Campanhas Pendentes: ".$qtdcampanhas, "info", $nomeLog);
    $campanhas = $result;
    $result = null;
    foreach ($campanhas as $rowcamp) { // tratamento para cada campanha
    // LISTA SMS PENDENTES
        $sms = listaSMSPend($conn, $rowcamp['campanha_id']);
        $qtdsmspend = count($sms);
        logMsg( "Campanha: ".$rowcamp['campanha_id']." - Qtd SMS Pendentes: ".$qtdsmspend, "info", $nomeLog);
        if ($qtdsmspend = 0){
            continue;//pula processamento da campanha se não houver sms pendentes
        }
        foreach ($sms as $rowsms) { // tratamento para cada mensagem
            /* DEBUG 
            echo "ID_SMS: ".$rowsms[0]." <br>"; //sms_id
            echo "Tipo Cliente: ".$rowsms[1]." <br>"; //Campanha_Usuario_Cliente_TipoCliente_tipocliente_id
            echo "Categ Cliente: ".$rowsms[2]." <br>"; //Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id
            echo "CNPJ: ".$rowsms[3]." <br>"; //Campanha_Usuario_Cliente_cliente_CNPJ
            echo "User id: ".$rowsms[4]." <br>"; //Campanha_Usuario_usuario_id
            echo "Camp id: ".$rowsms[5]." <br>"; //Campanha_campanha_id
            echo "Status camp: ".$rowsms[6]." <br>"; //Campanha_StatusCampanha_statuscampanha_id
            echo "Status sms: ".$rowsms[7]." <br>"; //StatusSMS_statussms_id
            echo "Fone: ".$rowsms[8]." <br>"; //sms_fone
            echo "Nome: ".$rowsms[9]." <br>"; //sms_nome
            echo "DataHora: ".$rowsms[10]." <br>";//sms_datahora
            echo "Operadora: ".$rowsms[11]." <br>";//sms_operadora
            echo "Mensagem: ".$rowsms[12]." <br>";//sms_mensagem
            echo "<br>";
            */
            // ENVIA SMS
            // verifica porta livre
            $res_porta = verifica_portas($conn, $tempoespera, $qtdsmspermitida);
            if ($res_porta == null){
                logMsg("Sem portas disponíveis para envio", "info", $nomeLog);
                break 2;
            }else{
                $porta = $res_porta['porta_num'];
                $gateway = $res_porta['Gateway_gateway_id'];
                $qtdemuso = $res_porta['porta_qtdemuso'];
                $utilizado_mes = $res_porta['porta_utilizado_mes'];
                /* DEBUG 
                echo " <br> ==== Porta livre === <br>";
                echo "Porta: ".$porta."<br>";
                echo "Gateway: ".$gateway."<br>";
                echo "Tempo: ".$tempoespera."<br>";
                echo "<br>";
            
                /**/ 
                // Monta dados de envio na estrutura da API da chipeira
                //              Parametros(Mensagem   , Numero    , IDUsuario , Porta , Encode   );
                $send_data = send_sms_data($rowsms['sms_mensagem'], $rowsms['sms_fone'], $rowsms['Campanha_Usuario_usuario_id'], (int)$porta, "unicode");
                /* DEBUG 
                print_r($send_data);
                echo "<br>";
               */

                // Envio dos dados
                // ****** comentar para não enviar msg ******
                $resultEnvio = post_data($send_url, $send_data, $username, $password);
                /* DEBUG 
                    $resultEnvio[0] = 200;
                    $resultEnvio[1] = "ERROXXX";
                /**/
                if ($resultEnvio[0] == 200) {
                  //Enviado para fila de envio da chipeira/porta
                
                  /* DEBUG 
                    echo " <br>Entrou no Result 200 <br>";
                    echo $resultEnvio[1];
                    */
                    $idsms = $rowsms['sms_id']; 
                    $idstatus = 2; // enviando
                    $resupd = alteraSMSPend($conn, $idsms, $idstatus, $porta, $gateway);
                    if ($resupd){
                        //echo "Erro ao alterar tabela de SMS_pendentes <br>";
                        logMsg("Erro ao alterar tabela de SMS_pendentes - idSMS: ".$idsms." idStatus: ".$idstatus." porta: ".$porta." gateway: ".$gateway, "error", $nomeLog);
                    
                    } //else  echo "Se chegou aqui alterou o status da SMS_PENDENTES com sucesso! <br>";

                    //atualiza qtd em uso
                    if($qtdemuso < $qtdsmspermitida){
                        ++$qtdemuso;
                    } else
                    {
                        $qtdemuso = 1;
                    }
                    ++$utilizado_mes;
                    $resupd = atualizaporta($conn, $gateway, $porta, $qtdemuso, $utilizado_mes);
                    if($resupd){
                        //echo "Erro ao alterar tabela Porta <br>";
                        logMsg("Erro ao alterar tabela Porta - Gateway: ".$gateway." Porta: ".$porta." QtdEmUso: ".$qtdemuso, "error", $nomeLog);
                    } //else  echo "Se chegou aqui alterou a porta com sucesso
                } else { // Ocorrido erro de envio para fila da chipeira
                    logMsg("Erro para fila de envio. Msg: ".$resultEnvio[1] , "error", $nomeLog);
                    //echo "Se chegou aqui, o código é FODA!";
                }

            }// Fim Se do verificaportas
        }// Fim ForEach SMS
    }// Fim ForEach Campanha
    //echo "<br>";
?>