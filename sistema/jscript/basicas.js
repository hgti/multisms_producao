$(document).ready(function() {
    limite_textarea();
});

function limite_textarea(valor) {
    quant = 160;
    total = valor.length;
    if (total <= quant) {
        resto = quant - total;
        document.getElementById('cont').innerHTML = resto;
    } else {
        document.getElementById('msg').value = valor.substr(0, quant);
    }
}

function reloadCss() {
    //Força reload do CSS para não precisar apertar Ctrl + F5
    /*
    for (var link of document.querySelectorAll("link[rel=stylesheet]")) {
        link.href = link.href.replace(/\?.*|$/, "?ts=" + new Date().getTime())
    }
    */
    //inline da versao acima
    //for (var link of document.querySelectorAll("link[rel=stylesheet]")) link.href = link.href.replace(/\?.*|$/, "?ts=" + new Date().getTime());
    //utilizar se necessário
}