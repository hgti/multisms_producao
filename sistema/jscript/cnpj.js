//<?echo mascara('555433323774','+XX (XX) XXXX-XXXX');

function mascara($string, $mascara) {
    $return = '';
    $j = '0';
    for ($i = '0'; $i < strlen($mascara); $i++) {
        if ($mascara[$i] != 'X') $return = $mascara[$i];
        else {
            $return = $string[$j];
            $j++;
        }
    }
    return $return;
}