<?php
require '../config.php';
require 'db/campanha.php';
require 'db/sms.php';
require 'db/sms_pendentes.php';
require 'db/usuario.php';
require 'php/valida.php';

// recupera registro do usuario
//$_SESSION['login'] = 'teste'; // remover para teste integrado

if (!validaLogin()){
    header("Location: ../proibido.html");
}
$login = $_SESSION['login']; 
$result = buscaUsuario($conn, $login);
$user_id = $result['usuario_id'];

// lista id e nome da CAMPANHA
$resultcampanha = listaUltimaCampanhaMes($conn, $result['Cliente_cliente_CNPJ']);
$result = listaCampanhasMes($conn, $result['Cliente_cliente_CNPJ']);
//$listaCampanhas = mysqli_fetch_all($result); // verificar se é assim que se carrega para o Select no form
/*$data = [];
while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $data[] = $row;
}*/

$idcampanha = $resultcampanha['campanha_id'];

// lista qtd SMS enviados
//a verificar como seta o valor de $idcampanha
//$qtdEnviados = contaSMS($conn, $idcampanha, 3);

// lista qtd SMS falha
//$qtdFalha = contaSMS($conn, $idcampanha, 4);

// lista qtd SMS_PENDENTES
//$qtdPendentes = contaPendporCamp($conn, $idcampanha, 1);

/* DEBUG 
echo "Login: ".$_SESSION['login']."<br>";
ECHO "Campanha: ".$idcampanha." <br>";
echo "CNPJ: ".$resultcampanha['Usuario_Cliente_cliente_CNPJ']." <br>";

ECHO "Enviados: ".$qtdEnviados." <br>";
ECHO "Falha: ".$qtdFalha." <br>";
ECHO "Pendentes: ".$qtdPendentes." <br>";
ECHO " <br>";
/**/

require "index.html";
?>
