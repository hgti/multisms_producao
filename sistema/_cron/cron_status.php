<?php
/*
Tem como objetivo verificar a base SMS_PENDENTES, com status "Enviando" e verificar se a mensagem já foi enviada. 
Caso positivo, insere os dados na base SMS
*/
/*
require '../config.php';
require '../API/param.php';
require '../API/funcoes.php';
require '../API/postget.php';
require '../db/sms_pendentes.php';
require '../db/sms.php';
require '../php/logs.php';
*/
$nomeLog = "CronStatus_".date('dmY').".log";
//echo 'CronStatus <br>';
// VER SMS_PENDENTES
$result = buscaSMSEnviando($conn);
$qtdsmspend = count($result);
logMsg( "Qtd SMS Enviando: ".$qtdsmspend, "info", $nomeLog);
$sms = $result;
$qtdgravados = 0;
$qtdenviando = 0;

foreach ($sms as $rowsms) { // tratamento para cada mensagem
    // verifica envio na chipeira
    $number = $rowsms['sms_fone'];
    $port = busca_porta($conn, $rowsms['Porta_Gateway_gateway_id'], $rowsms['Porta_porta_id']);
    $time_after = null;
    $time_before = null;
    $user_id = $rowsms['Campanha_Usuario_usuario_id'];

    /* DEBUG
    echo "<br>";
    echo "Number: ".$number."<br>";
    echo "Port: ".$port."<br>";
    echo "User ID: ".$user_id."<br>";
    echo "<br>";
    */
    //                   query_sms_result_data($number, $port, $time_after, $time_before, $user_id)
    $query_result_data = query_sms_result_data($number, $port, $time_after, $time_before, $user_id); // para resultado do envio dos dados para a chipeira
//    $res = post_data($result_url, $query_result_data, $username, $password); -- REMOVER PARA PRODUÇÃO
//    $r1 = json_decode($res[1], true);

    $r1["result"][0]["status"] = "DELIVERED"; // Linha de teste - retirar para produção

    if (!($r1["result"][0]["status"] == "SENDING")) {
        //Status para gravação na SMS: FAILED, SENT_OK, DELIVERED
        //echo "Entrou no SENDING <BR>";
        // Dados para inserção
        $id_sms = 0; //AUTOINCREMENT
        $idtipocliente = $rowsms['Campanha_Usuario_Cliente_TipoCliente_tipocliente_id'];
        $categcliente = $rowsms['Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id']; 
        $usuariocnpj = $rowsms['Campanha_Usuario_Cliente_cliente_CNPJ'];
        $idusuario = $rowsms['Campanha_Usuario_usuario_id'];
        $idcampanha = $rowsms['Campanha_campanha_id'];
        $statuscampanha = $rowsms['Campanha_StatusCampanha_statuscampanha_id'];
        if (($r1["result"][0]["status"] == "DELIVERED")){
            $statussms = 3; // alteração para status "Enviado"
        } else
        {
            $statussms = 4; // alteração para status "Falha"
        }
        $porta_id = $rowsms['Porta_porta_id'];
        $gtw = $rowsms['Porta_Gateway_gateway_id'];
        $smsfone = $rowsms['sms_fone'];
        $smsnome = $rowsms['sms_nome'];
        $smsoperadora = $rowsms['sms_operadora'];
        $smsmensagem = $rowsms['sms_mensagem'];


/*DEBUG
echo 'ID_SMS: '.$id_sms.'<br>';
echo 'TIPO CLIENTE: '.$idtipocliente.'<br>';
echo 'CATEGORIA :'.$categcliente.'<br>';
echo 'CNPJ: '.$usuariocnpj.'<br>';
echo 'ID USUARIO: '.$idusuario.'<br>';
echo 'ID CAMPANHA: '.$idcampanha.'<br>';
echo 'STATUS CAMP: '.$statuscampanha.'<br>';
echo 'STATUS SMS: '.$statussms.'<br>';
echo 'PORTA ID: '.$porta_id.'<br>';
echo 'GTW: '.$gtw.'<br>';
echo 'FONE: '.$smsfone.'<br>';
echo 'NOME: '.$smsnome.'<br>';
echo 'OPERADORA: '.$smsoperadora.'<br>';
echo 'MENSAGEM: '.$smsmensagem.'<br>';
*/

        $resInsert = insereSMS($conn, $id_sms, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, $porta_id, $gtw, $smsfone, $smsnome, $smsoperadora, $smsmensagem);
        if (($resInsert == TRUE)){
            //echo "Inclusão realizada com sucesso!<BR>";
            ++$qtdgravados;
            //if($idcampanha == 99){
            $resdelete = deletaPendPorId($conn, $rowsms['sms_id']);
            if ($resdelete == FALSE) {
            //echo "Deleção com sucesso <br>";
            } else {
                logMsg("Deleção na base SMS_Pendentes. Campanha: ".$idcampanha, "error", $nomeLog);
            //echo "Deleção com erro <br>";
            }

            //}
        } else{ 
            //echo "Deu ruim na inclusão do SMS";
            logMsg( "Erro na gravação SMS. CNPJ: ".$usuariocnpj." Campanha: ".$idcampanha." Fone: ".$smsfone." id_smsPend:  ".$rowsms['sms_id'], "error", $nomeLog);
        }
    } else {
        //echo "Entrou no else do SENDING <BR>";
        ++$qtdenviando;
        logMsg( "SMS com status Enviando: ".$usuariocnpj." Campanha: ".$idcampanha." Fone: ".$smsfone." id_smsPend:  ".$rowsms['sms_id'], "warning", $nomeLog);
    }

} //fim ForEach

logMsg("--- Gravados: ".$qtdgravados. "  ---", "info", $nomeLog);

?>