<?php
/*
Tem o objetivo de comparar a quantidade de mensagens enviadas com as pendentes. 
Caso tenha a mesma quantidade, remove da SMS_PENDENTES e atualiza CAMPANHAS
*/
/*
require '../config.php';
require '../db/campanha.php';
require '../db/sms_pendentes.php';
require '../db/sms.php';
require '../php/logs.php';
*/
$nomeLog = "CronCampanha_".date('dmY').".log";
//echo 'CronCampanha <br>';

// LISTA CAMPANHAS
$result = listaCampanhaPend($conn);
$campanhas = $result;
$qtdcamppend = count($result);
logMsg( "Qtd Campanhas pendentes: ".$qtdcamppend, "info", $nomeLog);
//echo "Quantidade de linhas=> ".mysqli_num_rows($result)."<br><br>";
$result = null;
foreach ($campanhas as $rowcamp) { // tratamento para cada campanha
    $idcampanha = $rowcamp['campanha_id'];
    if ($idcampanha == 99){
        continue;
    }
    $contaPend = contaPendporCamp($conn, $idcampanha, 1);
    $contaPend += contaPendporCamp($conn, $idcampanha, 2);
    $contaSMS = contaSMS($conn, $idcampanha, 3);
    /* DEBUG 
    echo "<br> Conta Pend total: ".$contaPend."<br>";
    echo "<br> Conta SMS total: ".$contaSMS."<br>";
    /* */
    logMsg( "Campanha: ".$idcampanha." - SMS_Pendentes: ".$contaPend." SMS: ".$contaSMS, "info", $nomeLog);
    if ($contaPend <= 0){
        $nomecampanha = $rowcamp['campanha_nome'];
        $statuscampanha = 2;// Status "Finalizada"
        $resAlteraCampanha = alteraCampanha($conn, $idcampanha, $nomecampanha, $statuscampanha);
        if ($resAlteraCampanha == FALSE) {
            //echo "Alteração de Campanha com sucesso <br>";
        } else {
            //echo "Alteração de Campanha com erro <br>";
            logMsg("Erro na alteração de Campanha: ".$idcampanha, "error", $nomeLog);
        }

        $resAlteraSMS = alteraSMS($conn, $idcampanha, $statuscampanha);
        if ($resAlteraSMS == FALSE) {
            //echo "Alteração de SMS com sucesso <br>";
        } else {
            logMsg("Erro na alteração de SMS. Campanha: ".$idcampanha, "error", $nomeLog);
            //echo "Alteração de SMS com erro <br>";
        }

        /*$res_Deleta = deletaPend($conn, $idcampanha);
        if ($res_Deleta == FALSE) {
            //echo "Deleção com sucesso <br>";
        } else {
            logMsg("Deleção na base SMS_Pendentes. Campanha: ".$idcampanha, "error", $nomeLog);
            //echo "Deleção com erro <br>";
        }*/
    } 

}//Fim Foreach
//echo "Fim do CRON Campanha! \o/ <br>";

?>