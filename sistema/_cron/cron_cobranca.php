<?php
/*PROCESSO BATCH PARA ALIMENTACAO DA BASE DE CONSUMO

*/
    $clientes = listaClientesAtivos($conn);
    foreach ($clientes as $rowclientes) {
        // monta CONSUMO
        $consumo_status = 1;
        $categoria_cliente = $rowclientes['CategoriaCliente_categoriacliente_id'];
        $tipo_cliente = $rowclientes['TipoCliente_tipocliente_id'];
        $resultprecos = buscaPrecos($conn, $rowclientes['cliente_CNPJ']);
        $precos_id = $resultprecos['precos_id'];
        $preco = $resultprecos['precos_val'];
        $consumo_qtd_usada = contaSMSporCNPJ($conn, $rowclientes['cliente_CNPJ']);
        $consumo_valor = $consumo_qtd_usada * $preco;

        //busca registro do mes atual. Se não encontra, insere
        $resultconsumo = buscaConsumo($conn, $rowclientes['cliente_CNPJ']);

/*DEBUG 
echo 'consumo_status :' .$consumo_status.'<br>';
echo 'CNPJ :' .$rowclientes['cliente_CNPJ'].'<br>';
echo 'Categoria_cliente :' .$categoria_cliente.'<br>';
echo 'Tipo_cliente :' .$tipo_cliente.'<br>';
echo 'precos_id :' .$precos_id.'<br>';
echo 'qtd_usada :' .$consumo_qtd_usada.'<br>';
echo 'consumo_valor :' .$consumo_valor.'<br>';
echo 'consumo_id :' .$resultconsumo['consumo_id'].'<br>';
/**/
        if(count($resultconsumo) > 0 || $resultconsumo != null){
            alteraConsumo($conn, $resultconsumo['consumo_id'], $consumo_valor, $consumo_qtd_usada);    
        }else{
            insereConsumo($conn, $consumo_status, $rowclientes['cliente_CNPJ'], $categoria_cliente, $tipo_cliente, $precos_id, $consumo_qtd_usada, $consumo_valor);
        }
    }
?>