<?php
/*
Cron centralizador de processamento. FREQUENCIA: DE 5 EM 5 MINUTOS
11 / 05 / 2017
Processos realizados:
 - cron_envio
 - cron_status
 - cron_campanha
 - cron_cobranca

*/

require 'config.php';
require 'campanha.php';
require 'sms_pendentes.php';
require 'sms.php';
require 'erros.php';
require 'porta.php';
require 'cliente.php';
require 'precos.php';
require 'consumo.php';
require 'param.php';
require 'funcoes.php';
require 'postget.php';
require 'logs.php';

//MODULO DE ENVIO
    $totalportas = contaTotalPortas($conn);
    $capacidadetotal = $totalportas * $capacidadechip;
    $limite = $capacidadetotal - $gordura;
    $emuso = contaPend($conn, 1);

/* DEBUG
    echo 'Em uso: '.$emuso.'<br>';
    echo 'Limite: '.$limite.'<br>';

*/
    if($emuso < $limite){ // trava de processamento baseado no limite de gordura definido
        //Verifica se o processo de envio já finalizou. Se sim, finaliza a Campanha
        require 'cron_campanha.php';
        //Envia os SMS pendentes
        require 'cron_envio.php';
        //Verifica os SMS enviados e carrega na base de SMS
        require 'cron_status.php';
    }

// MODULO DE COBRANCA
    require 'cron_cobranca.php';

// MODULO DE RESTART DE QTD UTILIZADA/MES POR PORTA
/*    $verifreset = verificaResetPorta($conn, $gateway);
    if ((date('d') == 1) || ($verifreset == 1)){
        require 'cron_reset_portas.php';
    }
*/

// atualização de portas. Se demora mais que o dobro do tempo de espera, zera contadores
$hora = '00';
$min = $minespera;
$min = (int)$min * 2;
$seg = '00';
$tempo = $hora.':'.(string)$min.':'.$seg;
//echo 'tempo: '.$tempo;
zeraPortas($conn, $tempo);
?>