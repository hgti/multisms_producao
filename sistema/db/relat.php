<?php
/*  LISTA DADOS PARA RELATORIOS / TELAS COM MESMO LAYOUTS
*/

function relatcampanhas($conn, $cnpj){

$sql = "SELECT  date_format(T.DATA, '%d/%m/%Y')	AS 'DATA',
                T.CAMPANHA_ID 		    AS CAMPANHA,
                T.CAMPANHA_NOME         AS CAMPANHA_NOME,
		        SUM(T.SMS_ENVIADOS)     AS ENVIADOS,
                SUM(T.SMS_FALHAS) 	    AS FALHAS,
                SUM(T.SMS_PENDENTES) 	AS PENDENTES,
                T.STATUS_CAMP			AS 'STATUS',
                T.USUARIO				AS USUARIO
        FROM
    (
        (select campanha_data       AS 'DATA',
                campanha_id         AS CAMPANHA_ID,
                campanha_nome       AS CAMPANHA_NOME,
		        count(sms1.sms_id)  AS  SMS_ENVIADOS,
                0 			        AS SMS_FALHAS ,
		        0 			        AS SMS_PENDENTES,
        statuscampanha.statuscampanha_descricao AS STATUS_CAMP,
        usuario.usuario_nome        AS USUARIO
        from campanha 
    INNER JOIN sms as sms1 on campanha.campanha_id = sms1.Campanha_campanha_id  
	    AND campanha.Usuario_usuario_id = sms1.Campanha_Usuario_usuario_id
	    AND campanha.Usuario_Cliente_cliente_CNPJ = sms1.Campanha_Usuario_Cliente_cliente_CNPJ
        AND campanha.Usuario_Cliente_CategoriaCliente_categoriacliente_id = sms1.Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id
        AND campanha.Usuario_Cliente_TipoCliente_tipocliente_id = sms1.Campanha_Usuario_Cliente_TipoCliente_tipocliente_id
        AND campanha.StatusCampanha_statuscampanha_id = sms1.Campanha_StatusCampanha_statuscampanha_id
	    AND sms1.StatusSMS_statussms_id = 3
    INNER JOIN statuscampanha on campanha.StatusCampanha_statuscampanha_id = statuscampanha.statuscampanha_id
    INNER JOIN usuario on campanha.Usuario_usuario_id = usuario.usuario_id 
	    AND campanha.Usuario_Cliente_cliente_CNPJ = usuario.Cliente_cliente_CNPJ
        AND campanha.Usuario_Cliente_TipoCliente_tipocliente_id = usuario.Cliente_TipoCliente_tipocliente_id
        AND campanha.Usuario_Cliente_CategoriaCliente_categoriacliente_id = usuario.Cliente_CategoriaCliente_categoriacliente_id
    WHERE campanha.Usuario_Cliente_cliente_CNPJ = ".$cnpj." 
    GROUP BY CAMPANHA_ID
    )
UNION

        (select campanha_data       AS 'DATA',
                campanha_id 	    AS CAMPANHA_ID,
                campanha_nome       AS CAMPANHA_NOME,
		        0 			        AS SMS_ENVIADOS,
                count(sms2.sms_id)  AS SMS_FALHAS,
                0 			        AS SMS_PENDENTES,
                statuscampanha.statuscampanha_descricao AS STATUS_CAMP,
		        usuario.usuario_nome AS USUARIO
        from campanha 
        INNER JOIN sms as sms2 on campanha.campanha_id = sms2.Campanha_campanha_id  
	        AND campanha.Usuario_usuario_id = sms2.Campanha_Usuario_usuario_id
	        AND campanha.Usuario_Cliente_cliente_CNPJ = sms2.Campanha_Usuario_Cliente_cliente_CNPJ
            AND campanha.Usuario_Cliente_CategoriaCliente_categoriacliente_id = sms2.Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id
            AND campanha.Usuario_Cliente_TipoCliente_tipocliente_id = sms2.Campanha_Usuario_Cliente_TipoCliente_tipocliente_id
            AND campanha.StatusCampanha_statuscampanha_id = sms2.Campanha_StatusCampanha_statuscampanha_id
	        AND sms2.StatusSMS_statussms_id = 4
        INNER JOIN statuscampanha on campanha.StatusCampanha_statuscampanha_id = statuscampanha.statuscampanha_id
        INNER JOIN usuario on campanha.Usuario_usuario_id = usuario.usuario_id 
	        AND campanha.Usuario_Cliente_cliente_CNPJ = usuario.Cliente_cliente_CNPJ
            AND campanha.Usuario_Cliente_TipoCliente_tipocliente_id = usuario.Cliente_TipoCliente_tipocliente_id
            AND campanha.Usuario_Cliente_CategoriaCliente_categoriacliente_id = usuario.Cliente_CategoriaCliente_categoriacliente_id
        WHERE campanha.Usuario_Cliente_cliente_CNPJ = ".$cnpj." 
        GROUP BY CAMPANHA_ID
)
UNION 
        (select campanha_data       AS 'DATA',
                campanha_id 	    AS CAMPANHA_ID,
                campanha_nome       AS CAMPANHA_NOME,
		        0 			        AS ENVIADOS,
                0 			        AS SMS_FALHAS,
                count(sms_pendentes.sms_id) AS PENDENTES, 
                statuscampanha.statuscampanha_descricao AS STATUS_CAMP,
		        usuario.usuario_nome AS USUARIO
        FROM campanha
        INNER JOIN sms_pendentes on campanha.campanha_id = sms_pendentes.Campanha_campanha_id AND campanha.Usuario_Cliente_cliente_CNPJ = sms_pendentes.Campanha_Usuario_Cliente_cliente_CNPJ
	        AND sms_pendentes.StatusSMS_statussms_id = 1
        INNER JOIN statuscampanha on campanha.StatusCampanha_statuscampanha_id = statuscampanha.statuscampanha_id
        INNER JOIN usuario on campanha.Usuario_usuario_id = usuario.usuario_id 
	        AND campanha.Usuario_Cliente_cliente_CNPJ = usuario.Cliente_cliente_CNPJ
            AND campanha.Usuario_Cliente_TipoCliente_tipocliente_id = usuario.Cliente_TipoCliente_tipocliente_id
            AND campanha.Usuario_Cliente_CategoriaCliente_categoriacliente_id = usuario.Cliente_CategoriaCliente_categoriacliente_id
        WHERE campanha.Usuario_Cliente_cliente_CNPJ = ".$cnpj." 
        GROUP BY CAMPANHA_ID
        ) 
) AS T
GROUP BY T.CAMPANHA_ID
ORDER BY T.DATA DESC, T.CAMPANHA_ID ASC
";

	$stmt = $conn->query($sql);
    $stmt->execute();
    return $stmt;
}

function relatsms($conn, $idcampanha){

$sql = "SELECT 	T.CAMPANHA AS CAMPANHA_ID, 
                T.CAMPANHA_NOME AS CAMPANHA_NOME,
		        T.SMS_ID AS ID,
                T.FONE AS FONE,
                T.NOME AS NOME,
                T.DATAHORA AS DATAHORA,
                T.STATUS_SMS AS STATUS,
                T.MENSAGEM AS MENSAGEM
        FROM 
    (
        (
        SELECT 	sms.Campanha_campanha_id AS CAMPANHA,
            campanha.campanha_nome AS CAMPANHA_NOME,
		    sms.sms_id AS SMS_ID,
		    sms_fone AS FONE, 
		    sms_nome AS NOME, 
            sms_datahora AS DATAHORA, 
            statussms.statussms_descricao AS STATUS_SMS,
            sms_mensagem AS MENSAGEM
        FROM sms
        INNER JOIN statussms on sms.StatusSMS_statussms_id = statussms.statussms_id
        INNER JOIN campanha on sms.Campanha_campanha_id = campanha.campanha_id
        ) 
    UNION
        (
        SELECT 	sms_pendentes.Campanha_campanha_id AS CAMPANHA,
                campanha.campanha_nome AS CAMPANHA_NOME,
		        sms_pendentes.sms_id AS SMS_ID,
		        sms_pendentes.sms_fone AS FONE, 
		        sms_pendentes.sms_nome AS NOME, 
                sms_pendentes.sms_datahora AS DATAHORA, 
                statussms.statussms_descricao AS STATUS_SMS,
                sms_mensagem AS MENSAGEM
        FROM sms_pendentes
        INNER JOIN statussms on sms_pendentes.StatusSMS_statussms_id = statussms.statussms_id
        INNER JOIN campanha on sms_pendentes.Campanha_campanha_id= campanha.campanha_id
        ) 
    ) AS T
    WHERE CAMPANHA = ".$idcampanha." 
    ORDER BY SMS_ID";

	$stmt = $conn->query($sql);
    $stmt->execute();
	return $stmt;

}
?>