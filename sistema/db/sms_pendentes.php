<?php
/*
ACESSOS DA TABELA SMS

*** ALTERACOES:
	HSC - 24/05/2017 - PDO 
		- contaPend
		- insereSMSPend
		- listaSMSPend

*/

function recuperaMaxSMSPend($conn, $usuariocnpj){
	
	$sql = "select Max(sms_id) as lastid from sms_pendentes where Campanha_Usuario_Cliente_cliente_CNPJ = '".$usuariocnpj."'";
	
	if ($result = mysqli_query($conn, $sql)) {
		$row = mysqli_fetch_assoc($result);
		$lastid = $row['lastid'];
		return $lastid;
	}
	else 
	{
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}
}

//function insereSMS($conn, $id_sms, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, $smsfone, $smsnome, $smsoperadora, $smsmensagem) {
function insereSMSPend($conn, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, $porta, $gtw, $smsfone, $smsnome, $smsoperadora, $smsmensagem) {
$sql = "
INSERT INTO sms_pendentes
		(sms_id,
		Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
		Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
		Campanha_Usuario_Cliente_cliente_CNPJ,
		Campanha_Usuario_usuario_id,
		Campanha_campanha_id,
		Campanha_StatusCampanha_statuscampanha_id,
		StatusSMS_statussms_id,
		Porta_Gateway_gateway_id,
		Porta_porta_id,
		sms_fone,
		sms_nome,
		sms_datahora,
		sms_operadora,
		sms_mensagem)
VALUES ( 0,
		".$idtipocliente.",
		".$categcliente.",
		".$usuariocnpj.",
		".$idusuario.",
		".$idcampanha.",
		".$statuscampanha.",
		".$statussms.",
		".$gtw.",
		".$porta.",
		".$smsfone.",
		'".$smsnome."',
		current_timestamp(),
		'".$smsoperadora."',
		'".$smsmensagem."'
		);";

try {
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	//echo $stmt->rowCount();
	} 
catch(PDOException $e) {
  echo 'Error: ' . $e->getMessage();
	}
}


/*MASSIVE SELECT
Lista SMS pendentes por campanha
Parâmetros: $idcampanha
*/
function listaSMSPend($conn, $idcampanha) {
$sql = "SELECT 	sms_id,
				Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
				Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
				Campanha_Usuario_Cliente_cliente_CNPJ,
				Campanha_Usuario_usuario_id,
				Campanha_campanha_id,
				Campanha_StatusCampanha_statuscampanha_id,
				StatusSMS_statussms_id,
				sms_fone,
				sms_nome,
				sms_datahora,
				sms_operadora,
				sms_mensagem
		FROM 	sms_pendentes
		WHERE 	Campanha_campanha_id ='".$idcampanha."' 
		AND 	StatusSMS_statussms_id = 1
		ORDER BY sms_id;";
/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;
/*
	if ($result = mysqli_query($conn, $sql)) {
		return $result;
	}
*/
/* HSC - 24/05/2017 - FIM */
}

/* SINGLE SELECT
Parâmetros: 
$conn, $idsms
 */ 
function buscaSMSPend($conn, $idsms) {

$sql = "SELECT 	sms_id,
				Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
				Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
				Campanha_Usuario_Cliente_cliente_CNPJ,
				Campanha_Usuario_usuario_id,
				Campanha_campanha_id,
				Campanha_StatusCampanha_statuscampanha_id,
				StatusSMS_statussms_id,
				sms_fone,
				sms_nome,
				sms_datahora,
				sms_operadora,
				sms_mensagem
		FROM sms_pendentes
			WHERE sms_id = '".$idsms."';";

	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			echo "SMS não foi encontrado";
			return $row;
		}
		else
		{
			$row=mysqli_fetch_row($result);
			return $row;
		}
	}
}

/*
MASSIVE SELECT PARA CARREGAR SMS PENDENTES COM STATUS "ENVIANDO"
*/

function buscaSMSEnviando($conn) {

$sql = "SELECT 	sms_id,
				Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
				Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
				Campanha_Usuario_Cliente_cliente_CNPJ,
				Campanha_Usuario_usuario_id,
				Campanha_campanha_id,
				Campanha_StatusCampanha_statuscampanha_id,
				StatusSMS_statussms_id,
				Porta_Gateway_gateway_id,
				Porta_porta_id,
				sms_fone,
				sms_nome,
				sms_datahora,
				sms_operadora,
				sms_mensagem
		FROM sms_pendentes
			WHERE StatusSMS_statussms_id = 2;";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;
	/* HSC - 24/05/2017 - INCIO */
/*
	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			//echo "Não foram encontrados SMS pendentes com status 2";
			return $result;
		}
		else
		{
			return $result;
		}
	}
*/
/* HSC - 24/05/2017 - FIM */
}


/* UPDATE
Parametros: 
$conn, $idsms, $idcampanha, $

*/

function alteraSMSPend($conn, $idsms, $idstatus, $porta, $gtw) {
	$sql = "UPDATE sms_pendentes
			SET StatusSMS_statussms_id = ".$idstatus.",
				Porta_Gateway_gateway_id = ".$gtw.",
				Porta_porta_id = ".$porta.",
				sms_datahora = current_timestamp()
			WHERE sms_id = ".$idsms.";";

	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		return FALSE;
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
		return TRUE;
	}
}

/* Conta a quantidade de SMS Pentendes por Campanha
Parâmetro: idcampanha
Retorna um Inteiro
Caso não encontre, retorna 0
*/
function contaPendporCamp($conn, $idcampanha, $statussms){
	$sql = "SELECT 1  
			FROM sms_pendentes 
			WHERE Campanha_campanha_id = ".$idcampanha."
			AND StatusSMS_statussms_id = ".$statussms.";";

/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return count($result);
/*
	if ($result = mysqli_query($conn, $sql)) {	
		 $count = mysqli_fetch_row($result);
		return $count[0];
	} else 	{
		/* DEBUG  
		echo " === CONTA_PEND === <br> ";
		echo "Error: ". mysqli_error($conn)." <br>";

		return 0;
	}
*/
/* HSC - 24/05/2017 - FIM */
}


/* Deleta registros por campanha
Parâmetro: idcampanha
Retorna um booleano
*/
function deletaPend($conn, $idcampanha){
	$sql = "DELETE FROM sms_pendentes WHERE Campanha_campanha_id = ".$idcampanha.";";

/* HSC - 24/05/2017 - INCIO */
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
  		return TRUE;
	}
return FALSE;
}

function deletaPendPorId($conn, $id_sms){
	$sql = "DELETE FROM sms_pendentes WHERE sms_id = ".$id_sms.";";
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
  		return TRUE;
	}
	return FALSE;
}

function listaPendDia($conn){
	$sql = "SELECT count(sms_id) AS qtd_pend FROM sms_pendentes WHERE sms_datahora <= now();";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result["qtd_pend"];
}

function listaProcDia($conn){
	$sql = "SELECT count(sms_id) AS qtd_proc FROM sms_pendentes WHERE StatusSMS_statussms_id = 2 AND sms_datahora <= now();";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result["qtd_proc"];
}

function contaPend($conn, $statussms){
	$sql = "SELECT 1  
			FROM sms_pendentes 
			WHERE StatusSMS_statussms_id = ".$statussms.";";

	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return count($result);

}


?>