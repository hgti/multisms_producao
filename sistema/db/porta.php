<?php
/*
*** ALTERACOES:
	HSC - 24/05/2017 - PDO 
		- verifica_portas
*/
function verifica_portas($conn, $tempo, $qtdsmspermitida){
    $sql = "SELECT 	porta_num , 
					Gateway_gateway_id,
					porta_qtdemuso,
					porta_utilizado_mes
    	FROM porta 
        WHERE (porta_status = 1 
		AND Gateway_gateway_id < 9999
		AND (porta_qtdemuso >= ".$qtdsmspermitida." 
				AND (TimeDiff(current_timestamp(),porta_ultimoenvio) > '".$tempo."') 
			))
        OR (porta_status = 1 
		AND Gateway_gateway_id < 9999
		AND porta_qtdemuso < ".$qtdsmspermitida.")
        ORDER BY porta_id 
		LIMIT 1;";
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result;
}

function atualizaporta($conn, $gateway, $porta, $qtdemuso, $porta_utilizado_mes){
$sql = "UPDATE 	porta 
		SET 	porta_qtdemuso = ".$qtdemuso.",
				porta_ultimoenvio = DATE_ADD(current_timestamp(), INTERVAL -3 HOUR),
				porta_utilizado_mes = ".$porta_utilizado_mes." 
		WHERE Gateway_gateway_id = ".$gateway." 
		AND porta_num = ".$porta." ;";

	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
	}
}

function busca_porta($conn, $gateway, $porta_id){
	$sql = "SELECT porta_num FROM porta WHERE Gateway_gateway_id = ".$gateway." AND porta_id = ".$porta_id.";";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result;
}

function cancela_porta($conn, $gateway, $porta_id){
	$sql = "UPDATE porta SET porta_status = 0 WHERE Gateway_gateway_id = ".$gateway." AND porta_id = ".$porta_id.";";
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
	}
}

function zeraPortas($conn, $tempo){
	$sql = "UPDATE 	porta 
			SET 	porta_qtdemuso = 0, porta_ultimoenvio = current_timestamp()
			WHERE 	TimeDiff(current_timestamp(),porta_ultimoenvio) > '".$tempo."';";
		try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage();
	}
}

function contaTotalPortas($conn){
	$sql = "SELECT count(porta_id) AS contTotal FROM porta WHERE porta_id < 99 and porta_status = 1;";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result["contTotal"];
}

function contaPortasProcessando($conn){
	$sql = "SELECT count(porta_id) AS contProcessando FROM porta WHERE porta_id < 99 and porta_status = 1 AND (porta_qtdemuso > 0 and porta_qtdemuso < 6);";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result["contProcessando"];
}

function contaPortasFull($conn){
	$sql = "SELECT count(porta_id) AS contFull FROM porta WHERE porta_id < 99 and porta_status = 1 AND porta_qtdemuso = 6;";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result["contFull"];
}

function listaPorta($conn){
	$sql = "SELECT 	porta_id, 
					Gateway_gateway_id, 
					porta_num, 
					porta_qtdemuso 
			FROM 	porta 
			WHERE 	Gateway_gateway_id < 9999 
			AND		porta_status = 1 
			ORDER BY Gateway_gateway_id ASC, porta_id ASC;";
		
	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function verificaResetPorta($conn, $gateway){
	$sql = "SELECT porta_controle_reset FROM porta WHERE Gateway_gateway_id = ".$gateway.";";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['porta_controle_reset'];
}

function resetPortaMes($conn, $gateway){
$sql = "UPDATE porta SET porta_utilizado_mes = 0, porta_controle_reset = 0
		WHERE Gateway_gateway_id = ".$gateway.";";

		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
}
?>