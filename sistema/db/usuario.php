<?php
/*
ACESSOS DA TABELA USUARIO

*** ALTERACOES:
	HSC - 24/05/2017 - PDO 
		- buscaUsuario
*/

/* INSERT */
function insereUsuario($conn, $sql, $categoria, $tipo, $cnpj, $lastid, $n, $l, $s) {
	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) <> 0){
			echo "Este usuário ou endereço de e-mail já está registrado em nosso banco de dados!<BR>";
			return FALSE;
		}
		else{

			$sql = "INSERT INTO Usuario (Cliente_TipoCliente_tipocliente_id, Cliente_CategoriaCliente_categoriacliente_id, Cliente_cliente_CNPJ, usuario_id, usuario_nome, usuario_login, usuario_senha) 
			VALUES('".$categoria."','".$tipo."','".$cnpj."','".$lastid."','".$n."','".$l."','".$s."')";

			if ($result = mysqli_query($conn, $sql)) {
				echo "Cadastro realizado com sucesso";
				return TRUE;
			} else {
				echo "Erro no cadastro";
				return FALSE;
			}
		}
	}
}

/* SINGLE SELECT por Login
Parâmetros:
$conn - Conexão MySQLi
Retorno: objeto $row com o registro encontrado
*/

function buscaUsuario($conn, $l) {

		$sql = "SELECT
		Cliente_TipoCliente_tipocliente_id, 		
		Cliente_CategoriaCliente_categoriacliente_id, 
		Cliente_cliente_CNPJ, 
		usuario_id, 
		usuario_nome, 
		usuario_login, 
		usuario_senha
		FROM usuario
		WHERE
		usuario_login = '".$l."'";

/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	/* DEBUG 
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$cnt = count($result);
	if($cnt < 1){
		echo "Usuario não foi encontrado";
	}
	*/
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	
	return $result;
/*
	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			echo "Usuario não foi encontrado";
			return $row;
		}
		else
		{
			$row=mysqli_fetch_row($result);
			return $row;
		}
	}*/
/* HSC - 24/05/2017 - FIM */
}

/* MASSIVE SELECT por CNPJ
Parâmetros:
$conn - Conexão MySQLi
Retorno: objeto $row com o registro encontrado
*/

function buscaUsuarioCNPJ($conn, $cnpj) {

		$sql = "SELECT
		Cliente_TipoCliente_tipocliente_id, 
		Cliente_CategoriaCliente_categoriacliente_id, 
		Cliente_cliente_CNPJ, 
		usuario_id, 
		usuario_nome, 
		usuario_login, 
		usuario_senha
		FROM usuario
		WHERE
		usuario_login = '".$cnpj."'";

	if ($result = mysqli_query($conn, $sql)) {
			return $result;
	}
}

/* DELETE por ID
Parâmetros:
$conn - Conexão MySQLi
$id

Retorno: N/A
*/

function deletaUsuario($conn, $l) {
	$sql = "DELETE FROM usuario WHERE usuario_id = '".$id."'";

	if ($result = mysqli_query($conn, $sql)) {
		echo "Deletado com sucesso<BR>";
		return TRUE;
	}else
	{
		echo "Erro ao apagar o registro";
		return FALSE;
	}
}


/* UPDATE por ID
Parâmetros:
$conn - Conexão MySQLi
$id - Não altera
$n  - Nome
$l  - Login
$s  - Senha

Retorno: N/A
*/
function AlteraUsuario($conn, $id, $n, $l, $s) {
	$sql = 
	
	"UPDATE Usuario 
		SET 
			usuario_nome = '".$n."', 
			usuario_login = '".$l."', 
			usuario_senha = '".$s."'
	  WHERE usuario_id = '".$id."'";
	
		if ($result = mysqli_query($conn, $sql)) {
		echo "Alterado com sucesso";
		return TRUE;
	}else
	{
		echo "Erro ao apagar o registro";
		return FALSE;
	}
	
	
}

?>