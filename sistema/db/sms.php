<?php
/*
ACESSOS DA TABELA SMS

*** ALTERACOES:
	HSC - 24/05/2017 - PDO 
		- contaSMS
		- recuperaMax
		- insereSMS
		- alteraSMS
*/

function recuperaMax($conn, $usuariocnpj){
	
	$sql = "select Max(sms_id) as lastid from sms where Campanha_Usuario_Cliente_cliente_CNPJ = '".$usuariocnpj."'";
/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->query($sql);
	return $stmt->fetchColumn();;
/*
	if ($result = mysqli_query($conn, $sql)) {
		$row = mysqli_fetch_assoc($result);
		$lastid = $row['lastid'];
		return $lastid;
	}
	else 
	{
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}
*/
/* HSC - 24/05/2017 - FIM */
}

function insereSMS($conn, $id_sms, $idtipocliente, $categcliente, $usuariocnpj, $idusuario, $idcampanha, $statuscampanha, $statussms, $gtw, $porta, $smsfone, $smsnome, $smsoperadora, $smsmensagem) {

$sql = "
INSERT INTO sms
		(sms_id,
		Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
		Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
		Campanha_Usuario_Cliente_cliente_CNPJ,
		Campanha_Usuario_usuario_id,
		Campanha_campanha_id,
		Campanha_StatusCampanha_statuscampanha_id,
		StatusSMS_statussms_id,
		Porta_Gateway_gateway_id,
		Porta_porta_id,
		sms_fone,
		sms_nome,
		sms_datahora,
		sms_operadora,
		sms_mensagem)
VALUES (
		".$id_sms.",
		".$idtipocliente.",
		".$categcliente.",
		".$usuariocnpj.",
		".$idusuario.",
		".$idcampanha.",
		".$statuscampanha.",
		".$statussms.",
		".$gtw.",
		".$porta.",
		".$smsfone.",
		'".$smsnome."',
		current_timestamp(),
		'".$smsoperadora."',
		'".$smsmensagem."'
		);";

/* HSC - 24/05/2017 - INCIO */
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage().'<br>';
		return FALSE;
	}
	return TRUE;

/*
	if ($result = mysqli_query($conn, $sql)) {
		//echo "Inclusão realizada com sucesso!<BR>";
		echo mysqli_error($conn);
		return TRUE;
	}
	else {
		echo mysqli_error($conn);
		return FALSE;
		}
/*
/* HSC - 24/05/2017 - FIM */
}


/*MASSIVE SELECT

Parâmetros:

*/
function listaSMS($conn, $idcampanha, $datainicio, $datafim) {
$sql = "SELECT 	sms_id_sms,
				Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
				Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
				Campanha_Usuario_Cliente_cliente_CNPJ,
				Campanha_Usuario_usuario_id,
				Campanha_campanha_id,
				Campanha_StatusCampanha_statuscampanha_id,
				StatusSMS_statussms_id,
				Porta_Gateway_gateway_id,
				Porta_porta_id,
				sms_fone,
				sms_nome,
				sms_datahora,
				sms_operadora,
				sms_mensagem
		FROM sms
		WHERE	Campanha_campanha_id '".$idcampanha."'
		;";

	if ($result = mysqli_query($conn, $sql)) {
		return $result;
	}
}

/* SINGLE SELECT
Parâmetros: 
$conn, $idsms
 */ 
function buscaSMS($conn, $idsms) {

$sql = "SELECT 	sms_id_sms,
				Campanha_Usuario_Cliente_TipoCliente_tipocliente_id,
				Campanha_Usuario_Cliente_CategoriaCliente_categoriacliente_id,
				Campanha_Usuario_Cliente_cliente_CNPJ,
				Campanha_Usuario_usuario_id,
				Campanha_campanha_id,
				Campanha_StatusCampanha_statuscampanha_id,
				StatusSMS_statussms_id,
				Porta_Gateway_gateway_id,
				Porta_porta_id,
				sms_fone,
				sms_nome,
				sms_datahora,
				sms_operadora,
				sms_mensagem
		FROM sms
			WHERE sms_id = '".$idsms."';";

	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			//echo "SMS não foi encontrado";
			return $result;
		}
		else
		{
			//$row=mysqli_fetch_row($result);
			return $result;
		}
	}
}

/* UPDATE
Parametros: 
$conn, $idsms, $idcampanha, $

*/

function alteraSMS($conn, $idcampanha, $statuscampanha) {
	$sql = "UPDATE sms
			SET Campanha_StatusCampanha_statuscampanha_id = ".$statuscampanha."
			WHERE Campanha_campanha_id = '".$idcampanha."';";
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage().'<br>';
  		return TRUE;
	}
return FALSE;
}

/* Conta a quantidade de SMS
Parâmetro: idcampanha
Retorna um Inteiro
Caso não encontre, retorna 0

function contaSMS($conn, $idcampanha){           //REVISAR
	$sql = "SELECT count(sms_id) AS 'count_SMS' 
			FROM sms
			WHERE Campanha_campanha_id = ".$idcampanha.";";
	if ($result = mysqli_query($conn, $sql)) {	
		$count_pend =  mysqli_fetch_row($result);
		return $count_pend;
	} else 	{
		/* DEBUG  
		echo " === CONTA_PEND === <br> ";
		echo "Error: ". mysqli_error($conn)." <br>";
		/* 
		return 0;
	};
}
*/
function contaSMS($conn, $idcampanha, $statussms){
	$sql = "SELECT (1) 
			FROM sms
			WHERE Campanha_campanha_id = ".$idcampanha."
			  AND StatusSMS_statussms_id = ".$statussms.";";

/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return count($result);

/* 
	if ($result = mysqli_query($conn, $sql)) {	
		$count =  mysqli_fetch_row($result);
		return $count[0];
	} else 	{
		/* DEBUG  
		echo " === CONTA_PEND === <br> ";
		echo "Error: ". mysqli_error($conn)." <br>";

		return 0;
	}
*/
/* HSC - 24/05/2017 - FIM */
}

function contaSMSporCNPJ($conn, $usuariocnpj){
	$sql = "SELECT 	COUNT(sms_id)
			FROM 	sms
			WHERE 	Campanha_Usuario_Cliente_cliente_CNPJ = ".$usuariocnpj."
			AND		month(sms_datahora) = month(now())
			AND		year(sms_datahora) = year(now());";

	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return count($result);
}

function retornaMensagem($conn, $idcampanha){
	$sql = "SELECT sms_mensagem 
			FROM sms 
			WHERE Campanha_campanha_id = ". $idcampanha." 
			LIMIT 1;";
	$stmt = $conn->query($sql);
	$stmt->execute();
	$result =$stmt->fetch(PDO::FETCH_ASSOC);
	return $result;
}

?>