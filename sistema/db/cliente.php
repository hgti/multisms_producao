<?php
/*

*/
function insereCliente($conn, $cliente_CNPJ, $CategoriaClienteid, $tipocliente_id, $cliente_nome, $cliente_IM, $cliente_IE, $cliente_IsencaoIE, $cliente_CEP, $cliente_end, $cliente_end_num, $cliente_end_comp, $cliente_bairro, $cliente_cidade, $cliente_estado, $cliente_pais, $cliente_telefone, $cliente_email, $cliente_site){
	$sql = " INSERT INTO cliente
                (cliente_CNPJ,
                CategoriaCliente_categoriacliente_id,
                TipoCliente_tipocliente_id,
                cliente_nome,
                cliente_IM,
                cliente_IE,
                cliente_IsencaoIE,
                cliente_CEP,
                cliente_end,
                cliente_end_num,
                cliente_end_comp,
                cliente_bairro,
                cliente_cidade,
                cliente_estado,
                cliente_pais,
                cliente_telefone,
                cliente_email,
                cliente_site)
            VALUES
                (0,
                ".$cliente_CNPJ.",
                ".$CategoriaCliente_categoriacliente_id.", /*categoria 1 - Assessoria de Cobrança*/
                ".$TipoCliente_tipocliente_id.", /*1 - PJ / 2 - PF*/
                ".$cliente_nome.",
                ".$cliente_IM.",
                ".$cliente_IE.",
                ".$cliente_IsencaoIE.",
                ".$cliente_CEP.",
                ".$cliente_end.",
                ".$cliente_end_num.",
                ".$cliente_end_comp.",
                ".$cliente_bairro.",
                ".$cliente_cidade.",
                ".$cliente_estado.",
                ".$cliente_pais.",
                ".$cliente_telefone.",
                ".$cliente_email.",
                ".$cliente_site.");";
		
    try {
        $stmt = $conn->prepare($sql);
        $stmt->execute();
		//e		cho $stmt->rowCount();
    	}
    catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    	}
	
}

function deletaCliente($conn, $cliente_CNPJ){
    $sql = "DELETE FROM cliente WHERE cliente_CNPJ = '".$cliente_CNPJ."'";
    try {
        $stmt = $conn->prepare($sql);
        $stmt->execute();
		//e		cho $stmt->rowCount();
    	}
    catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    	}

}

function alteraCliente($conn, $cliente_CNPJ, $CategoriaClienteid, $tipocliente_id, $cliente_nome, $cliente_IM, $cliente_IE, $cliente_IsencaoIE, $cliente_CEP, $cliente_end, $cliente_end_num, $cliente_end_comp, $cliente_bairro, $cliente_cidade, $cliente_estado, $cliente_pais, $cliente_telefone, $cliente_email, $cliente_site){
    $sql = "UPDATE 	cliente 
	    	SET     CategoriaCliente_categoriacliente_id = ".$CategoriaClienteid.",
                    TipoCliente_tipocliente_id = ".$tipocliente_id.",
                    cliente_nome = ".$cliente_nome.",
                    cliente_IM = ".$cliente_IM.",
                    cliente_IE = ".$cliente_IE.",
                    cliente_IsencaoIE = ".$cliente_IsencaoIE.",
                    cliente_CEP = ".$cliente_CEP.",
                    cliente_end = ".$cliente_end.",
                    cliente_end_num = ".$cliente_end_num.",
                    cliente_end_comp = ".$cliente_end_comp.",
                    cliente_bairro = ".$cliente_bairro.",
                    cliente_cidade = ".$cliente_cidade.",
                    cliente_estado = ".$cliente_estado.",
                    cliente_pais = ".$cliente_pais.",
                    cliente_telefone = ".$cliente_telefone.",
                    cliente_email = ".$cliente_email.",
                    cliente_site = ".$cliente_site.";)";

    try {
        $stmt = $conn->prepare($sql);
        $stmt->execute();
		//e		cho $stmt->rowCount();
    	}
    catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    	}
}

function listaClientesAtivos($conn){
    $sql = "SELECT 
                cliente_CNPJ,
                CategoriaCliente_categoriacliente_id,
                TipoCliente_tipocliente_id,
                cliente_nome,
                cliente_IM,
                cliente_IE,
                cliente_IsencaoIE,
                cliente_CEP,
                cliente_end,
                cliente_end_num,
                cliente_end_comp,
                cliente_bairro,
                cliente_cidade,
                cliente_estado,
                cliente_pais,
                cliente_telefone,
                cliente_email,
                cliente_site
            FROM 
                cliente
            ORDER BY cliente_CNPJ;";
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;

}

function buscaCliente($conn, $cliente_CNPJ){
    $sql = "SELECT 
                cliente_CNPJ,
                CategoriaCliente_categoriacliente_id,
                TipoCliente_tipocliente_id,
                cliente_nome,
                cliente_IM,
                cliente_IE,
                cliente_IsencaoIE,
                cliente_CEP,
                cliente_end,
                cliente_end_num,
                cliente_end_comp,
                cliente_bairro,
                cliente_cidade,
                cliente_estado,
                cliente_pais,
                cliente_telefone,
                cliente_email,
                cliente_site
            FROM 
                cliente
            WHERE cliente_CNPJ = ".$cliente_CNPJ.";";
	
    $stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	return $result;
}

?>