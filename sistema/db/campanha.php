<?php
/*
ACESSOS DA TABELA CAMPANHA

*** ALTERACOES:
	HSC - 24/05/2017 - PDO 
		- listaCampanhasMes
		- recuperaMaxCamp
		- insereCampanha
		- listaCampanhaPend
		- alteraCampanha
*/



/* INSERE CAMPANHA
Parâmetros: 
$conn, $cnpj, $categ, $tipocliente, $id, $idfuncao
*/
function insereCampanha($conn, $idcampanha, $idusuario, $usuariocnpj, $categcliente, $idtipocliente, $statuscampanha, $nomecampanha) 
{
$sql = "INSERT INTO campanha 
		(campanha_id, 
		Usuario_usuario_id, 
		Usuario_Cliente_cliente_CNPJ, 
		Usuario_Cliente_CategoriaCliente_categoriacliente_id, 
		Usuario_Cliente_TipoCliente_tipocliente_id, 
		StatusCampanha_statuscampanha_id,
		campanha_nome, 
		campanha_data, 
		campanha_hora) 
		VALUES
		('".$idcampanha."',
		'".$idusuario."', 
		'".$usuariocnpj."', 
		'".$categcliente."', 
		'".$idtipocliente."', 
		'".$statuscampanha."', 
		'".$nomecampanha."',
		current_date(), 
		SELECT DATE_SUB(current_time(), INTERVAL 3 HOUR)
		);";
/* HSC - 24/05/2017 - INCIO */
try {
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	//echo $stmt->rowCount();
	} 
catch(PDOException $e) {
  echo 'Error: ' . $e->getMessage().'<br>';
	}
 	/*
	if ($result = mysqli_query($conn, $sql)) {
		//inclusao realizada com sucesso
		return FALSE;
	}
	else{
		/* DEBUG
			echo "Error: " . $sql . "<br>". mysqli_error($conn);
			echo "<br>";

		return TRUE;
		}
	}
	*/
/* HSC - 24/05/2017 - FIM */
}
/*REMOVE CAMPANHA 
Parâmetros: 
$conn, $id, $idfuncao
*/
function removeCampanha($conn, $idcampanha) {
$sql = "DELETE FROM campanha WHERE campanha_id = '".$idcampanha."';";

	if ($result = mysqli_query($conn, $sql)) {
		return FALSE;
	}
	else{
		echo "Remoção realizada com erro!<BR>";
		return TRUE;
		}
}


/*ALTERA CAMPANHA 
Parâmetros: 
$conn, $idcampanha, $nomecampanha, $statuscampanha
*/

function alteraCampanha($conn, $idcampanha, $nomecampanha, $statuscampanha) {
	$sql = "UPDATE campanha 
			SET campanha_nome = '".$nomecampanha."',
				StatusCampanha_statuscampanha_id = '".$statuscampanha."'
			WHERE campanha_id = '".$idcampanha."';";
/* HSC - 24/05/2017 - INCIO */

try {
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	//echo $stmt->rowCount();
	} 
catch(PDOException $e) {
  echo 'Error: ' . $e->getMessage();
  return TRUE;
	}
return FALSE;

/*
	if ($result = mysqli_query($conn, $sql)) {
		//echo "Alterado com sucesso <BR>";
		return FALSE;
	}else
	{
		//echo "Erro ao apagar o registro <BR>";
		return TRUE;
	}
*/
/* HSC - 24/05/2017 - FIM */
}

/* MASSIVE SELECT(1) DE CAMPANHA DO USUARIO POR MES
Parâmetros: 
$conn, $CNPJ
*/
function listaCampanhasMes($conn, $cnpj) {

		$sql = "SELECT
		campanha_id,
		Usuario_usuario_id,
		Usuario_Cliente_cliente_CNPJ,
		Usuario_Cliente_CategoriaCliente_categoriacliente_id,
		Usuario_Cliente_TipoCliente_tipocliente_id,
		StatusCampanha_statuscampanha_id,
		campanha_nome,
		campanha_data,
		campanha_hora
		FROM campanha
		WHERE
		Usuario_Cliente_cliente_CNPJ = '".$cnpj."' 
		ORDER BY campanha_id DESC ;";
/* 	AND	MONTH(campanha_data) = MONTH(current_date())  */
/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	return $stmt;
/*
	if ($result = mysqli_query($conn, $sql)) {
		return $result;
	}else{
		/* DEBUG 
			echo "Error: " . $sql . "<br>". mysqli_error($conn);
			echo "<br>";

		}
 HSC - 24/05/2017 - FIM */

}

function listaUltimaCampanhaMes($conn, $cnpj) {

		$sql = "SELECT
		campanha_id,
		Usuario_usuario_id,
		Usuario_Cliente_cliente_CNPJ,
		Usuario_Cliente_CategoriaCliente_categoriacliente_id,
		Usuario_Cliente_TipoCliente_tipocliente_id,
		StatusCampanha_statuscampanha_id,
		campanha_nome,
		campanha_data,
		campanha_hora
		FROM campanha
		WHERE
			Usuario_Cliente_cliente_CNPJ = '".$cnpj."' AND
			MONTH(campanha_data) = MONTH(current_date()) 
		ORDER BY campanha_id DESC 
		LIMIT 1;";

	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	return $result;
}


/* MASSIVE SELECT(2) DE CAMPANHA - lista campanhas em processamento
Parâmetros: 
$statuscampanha
*/
function listaCampanhaPend($conn) {

		$sql = "SELECT  campanha_id,
						campanha_nome
				FROM campanha
				WHERE StatusCampanha_statuscampanha_id = 1;"; 

/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result =$stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;
/*
	if ($result = mysqli_query($conn, $sql)) {
		return $result;
	}else{
		/* DEBUG 
			echo "Error: " . $sql . "<br>". mysqli_error($conn);
			echo "<br>";
	}*/
/* HSC - 24/05/2017 - FIM */
}


/* SINGLE SELECT DE CAMPANHA POR ID
Parâmetros: 
$conn, $idcampanha
*/
function buscaCampanhaId($conn, $idcampanha) {

	$sql = "SELECT
			campanha_id,
			Usuario_usuario_id,
			Usuario_Cliente_cliente_CNPJ,
			Usuario_Cliente_CategoriaCliente_categoriacliente_id,
			Usuario_Cliente_TipoCliente_tipocliente_id,
			StatusCampanha_statuscampanha_id,
			campanha_nome,
			campanha_data,
			campanha_hora
			FROM campanha
			WHERE campanha_id = '".$idcampanha."';";

/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	return $result;

/*
	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			//echo "<br> Campanha não encontrada <br>";
			return $row;
		}
		else
		{	
			//echo "<br> Campanha OK <br>";
			$row=mysqli_fetch_row($result);
			return $row;
		}
	}
*/
/* HSC - 24/05/2017 - INCIO */
}

/* MASSIVE SELECT DE CAMPANHA POR USUARIO
Parâmetros: 
$conn, $idusuario
*/
function buscaCampanhaUsuario($conn, $idusuario) {

	$sql = "SELECT
			campanha_id,
			Usuario_usuario_id,
			Usuario_Cliente_cliente_CNPJ,
			Usuario_Cliente_CategoriaCliente_categoriacliente_id,
			Usuario_Cliente_TipoCliente_tipocliente_id,
			StatusCampanha_statuscampanha_id,
			campanha_data,
			campanha_hora
			FROM campanha
			WHERE Usuario_usuario_id = '".$idusuario."';";

	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			//echo "Campanha não foi encontrada";
			return $result;
		}
		else
		{
			//$row=mysqli_fetch_row($result);
			return $result;
		}
	}
}


function recuperaMaxCamp($conn, $usuariocnpj){
	
	$sql = "select max(campanha_id) as 'lastid' from campanha where Usuario_Cliente_cliente_CNPJ = '".$usuariocnpj."'";
	/* HSC - 24/05/2017 - INCIO */
	$stmt = $conn->query($sql);
	return $stmt->fetchColumn();
	/*
	if ($result = mysqli_query($conn, $sql)) {
		$row = mysqli_fetch_assoc($result);
		$lastid = $row['lastid'];
		return $lastid;
	}
	else 
	{
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}*/
	/* HSC - 24/05/2017 - FIM */
}

function atualizaDataCampanha($conn, $idcampanha, $cnpj){
	$sql = "UPDATE  campanha 
			SET campanha_data = current_date(), 
        		campanha_hora = current_time() 
 			WHERE campanha_id =  '".$idcampanha."'
			  AND Usuario_Cliente_cliente_CNPJ = '".$cnpj."';";
	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage().'<br>';
  		return TRUE;
	}
return FALSE;
}

function cancelaCamp($conn, $idcampanha){
	$sql = "UPDATE campanha 
			SET StatusCampanha_statuscampanha_id = 3 
			WHERE campanha_id".$idcampanha.";";
	if ($result = mysqli_query($conn, $sql)) {
		return $result;
	}else{

	}
}

?>