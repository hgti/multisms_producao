<?php

function buscaConsumo($conn, $cliente_CNPJ){
    $sql = "SELECT  consumo_id,
                    Status_consumo_Statusconsumo_id,
                    Precos_Cliente_cliente_CNPJ,
                    Precos_Cliente_CategoriaCliente_categoriacliente_id,
                    Precos_Cliente_TipoCliente_tipocliente_id,
                    Precos_precos_id,
                    consumo_qtd_usada,
                    consumo_mes_referencia,
                    consumo_ano_referencia,
                    consumo_valor 
            FROM consumo 
            WHERE Precos_Cliente_cliente_CNPJ = ".$cliente_CNPJ."
            AND consumo_ano_referencia = year(now()) 
            AND consumo_mes_referencia = month(now()) ;";
	
    $stmt = $conn->prepare($sql);
	$stmt->execute();
    //echo '<br>RowCount: '.$stmt->rowCount().'<br>';
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
    $count = $stmt->rowCount();
	return array($result, $count);
}

function insereConsumo($conn, $consumo_status, $CNPJ, $categoria_cliente, $tipo_cliente, $precos_id, $consumo_qtd_usada, $consumo_valor){
    /*DEBUG*/
        echo '<br>';
        echo '0<br>';
        echo 'Consumo_status: '.$consumo_status.'<br>';
        echo 'CNPJ: '.$CNPJ.'<br>';
        echo 'Categ_Cliente: '.$categoria_cliente.'<br>';
        echo 'TipoCliente: '.$tipo_cliente.'<br>';
        echo 'Preços_id: '.$precos_id.'<br>';
        echo 'Qtd_Usada: '.$consumo_qtd_usada.'<br>';
        echo 'month <br>';
        echo 'year <br>';
        echo 'Consumo_Valor: '.$consumo_valor.'<br>';
    /**/

    $sql = "INSERT INTO consumo (
                consumo_id,
                Status_consumo_Statusconsumo_id,
                Precos_Cliente_cliente_CNPJ,
                Precos_Cliente_CategoriaCliente_categoriacliente_id,
                Precos_Cliente_TipoCliente_tipocliente_id,
                Precos_precos_id,
                consumo_qtd_usada,
                consumo_mes_referencia,
                consumo_ano_referencia,
                consumo_valor)
            VALUES (
                0,
                $consumo_status,
                $CNPJ,
                $categoria_cliente,
                $tipo_cliente,
                $precos_id,
                $consumo_qtd_usada,
                month(now()),
                year(now()),
                $consumo_valor 
                ); ";

	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage().'<br>';
  		return TRUE;
	}
    return FALSE;

}


function alteraConsumo($conn, $consumo_id, $consumo_valor, $consumo_qtd_usada){
	$sql = "UPDATE consumo 
            SET consumo_valor = ".$consumo_valor.", 
            consumo_qtd_usada = ".$consumo_qtd_usada."
            WHERE consumo_id = ".$consumo_id.";";

	try {
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		//echo $stmt->rowCount();
		} 
	catch(PDOException $e) {
  		echo 'Error: ' . $e->getMessage().'<br>';
  		return TRUE;
	}
return FALSE;

}

function listaConsumoMes($conn){
$sql = "SELECT  consumo_id,
		Status_consumo_Statusconsumo_id,
		Precos_Cliente_cliente_CNPJ,
		Precos_Cliente_CategoriaCliente_categoriacliente_id,
		Precos_Cliente_TipoCliente_tipocliente_id,
		Precos_precos_id,
		consumo_qtd_usada,
		consumo_mes_referencia,
		consumo_ano_referencia,
		consumo_valor
FROM 	consumo 
WHERE 	consumo_ano_referencia = year(now()) 
AND 	consumo_mes_referencia = month(now());";

    $stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function somaValorConsumoMes($conn){
$sql = "SELECT  sum(consumo_valor) 
        FROM    consumo 
        WHERE   consumo_ano_referencia = year(now()) 
        AND     consumo_mes_referencia = month(now());";

    $stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function contaConsumoMes($conn){
$sql = "SELECT  count(consumo_valor) AS 'EMUSO'
        FROM    consumo 
        WHERE   consumo_ano_referencia = year(now()) 
        AND     consumo_mes_referencia = month(now());";

    $stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	return $result['EMUSO'];
}

?>