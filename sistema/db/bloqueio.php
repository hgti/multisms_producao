<?php
/*
ACESSOS DA TABELA BLOQUEIO

*/

/* INSERT */

function inserebloqueio($conn, $usuarioid, $clientecnpj, $categclienteid, $tipoclienteid, $motbloq, $numero, $nome) {

// recuperando ultimo ID
$sql = "SELECT MAX(bloqueio_id) AS 'lastid' FROM BLOQUEIO";

	if ($result = mysqli_query($conn, $sql)) {
		$row = mysqli_fetch_assoc($result);
		$lastid = $row['lastid'];
		++$lastid;
		echo "lastid=".$lastid.".";
	} 
	else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}


$sql = "INSERT INTO bloqueio 
		(bloqueio_id, 
		Usuario_usuario_id, 
		Usuario_Cliente_cliente_CNPJ, 
		Usuario_Cliente_CategoriaCliente_categoriacliente_id, 
		Usuario_Cliente_TipoCliente_tipocliente_id, 
		MotivoBloqueio_id, 
		bloqueio_numero, 
		bloqueio_nome, 
		bloqueio_datahora) 
		VALUES(
		".$lastid.",
		".$usuarioid.",
		".$clientecnpj.",
		".$categclienteid.",
		".$tipoclienteid.",
		".$motbloq.",
		".$numero.",
		'".$nome."',
		DATETIME);";

	if ($result = mysqli_query($conn, $sql)) {
		echo "Cadastrado com sucesso<BR>";
		return TRUE;
	}
	else
	{
		echo "Erro no cadastro";
		return FALSE;
	}
}


/* MASSIVE SELECT por CNPJ do usuario
Parâmetros:
$conn - Conexão MySQLi
Retorno: objeto $row com o registro encontrado
*/

function listaBloqueios($conn, $cnpj){
$sql = "SELECT * FROM bloqueio WHERE Cliente_cliente_CNPJ = '".$cnpj."'";
	$result = mysqli_query($conn, $sql);
	return $result;
}

function BuscaBloqueio($conn, $bloqid){
	$sql = "SELECT 	bloqueio_id, 
					Usuario_usuario_id, 
					Usuario_Cliente_cliente_CNPJ, 
					Usuario_Cliente_CategoriaCliente_categoriacliente_id, 
					Usuario_Cliente_TipoCliente_tipocliente_id, 
					MotivoBloqueio_id, 
					bloqueio_numero, 
					bloqueio_nome, 
					bloqueio_datahora
			FROM bloqueio 
			WHERE bloqueio_id = '".$bloqid."'";
			
	if ($result = mysqli_query($conn, $sql)) {
		if (mysqli_num_rows($result) < 1){
			echo "Número não foi encontrado";
			return $row;
		}
		else
		{
			$row=mysqli_fetch_row($result);
			return $row;;
		}
	}
	
}



/* DELETE por ID
Parâmetros:
$conn - Conexão MySQLi
$id

Retorno: N/A
*/

function deletaUsuario($conn, $bloqid) {
	$sql = "DELETE FROM bloqueio WHERE bloqueio_id = '".$bloqid."'";
	
	if ($result = mysqli_query($conn, $sql)) {
		echo "Deletado com sucesso<BR>";
		return TRUE;
	}else
	{
		echo "Erro ao apagar o registro";
		return FALSE;
	}
}



?>