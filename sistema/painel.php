<?php
/*LISTAGEM ADMINISTRATIVA PARA TOMADAS DE DECISÃO*/
require '../config.php';
require 'db/sms_pendentes.php';
require 'db/porta.php';
require 'db/gateway.php';
require 'db/consumo.php';


$_SESSION['login'] = 'admin';//remover para integração

if ($_SESSION['login'] == 'admin'){
    $sms_usados = contaConsumoMes($conn);
    $sms_usados = (float)$sms_usados;
//    echo '$sms_usados:'.$sms_usados.'<br>';

    $smspendentes = listaPendDia($conn);//qtd em fila
    $smsprocessando = listaProcDia($conn);//qtd em processamento

    $totalportas = contaTotalPortas($conn);
    $qtdenviando = contaPortasProcessando($conn);
    $qtdfull = contaPortasFull($conn);
    $qtdociosos = $totalportas - ($qtdfull + $qtdenviando);
    /*DEBUG 
    echo '$totalportas: '.$totalportas.'<br>';
    echo '$qtdenviando: '.$qtdenviando.'<br>';
    echo '$qtdfull: '.$qtdenviando.'<br>';
    echo '$qtdociosos: '.$qtdenviando.'<br>';
    /**/
    $porcenviando = (100 * $qtdenviando)/$totalportas;
    $porcfull = (100 * $qtdfull)/$totalportas;
    $porcociosos = (100 * $qtdociosos)/$totalportas;

    $capacidadetotal = $totalportas * $capacidadechip;

    $qtddeenvio = $smspendentes / $capacidadetotal;
    $tempoestimado = ($qtddeenvio * (int)$minespera)/60;
/*
    echo 'Qtd de mensagens Pendentes: '.$smspendentes.'<br>';
    echo 'Qtd de mensagens Processando: '.$smsprocessando.'<br>';
    echo 'Tempo Estimado de Espera: '.$tempoestimado.' hora(s) <br><br>' ;

    echo 'Qtd Total:'.$totalportas.'<br>';
    echo 'Qtd Enviando:'.$qtdenviando.'<br>';
    echo 'Qtd Full:'.$qtdfull.'<br>';
    echo 'Qtd Ociosos:'.$qtdociosos.'<br><br>';

    echo '% Enviando: '.$porcenviando.'<br>';
    echo '% Full: '.$porcfull.'<br>';
    echo '% Ociosos: '.$porcociosos.'<br>';
*/
    $gateways = listaGateway($conn);
    $portas = listaPorta($conn);

}else{
    echo 'Acesso proibido';
}


require 'painel.html';
?>